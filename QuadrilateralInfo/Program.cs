﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadrilateralInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = 1;
            Console.WriteLine("Добро пожаловать в программу ЧетырёхугольникИнфо!");

            do {
                Console.Write("Введите Координату Х точки А: ");
                double xA = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите Координату Y точки А: ");
                double yA = Convert.ToDouble(Console.ReadLine());
                Point pointA = new Point(xA, yA);

                Console.Write("Введите Координату Х точки B: ");
                double xB = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите Координату Y точки B: ");
                double yB = Convert.ToDouble(Console.ReadLine());
                Point pointB = new Point(xB, yB);

                Console.Write("Введите Координату Х точки C: ");
                double xC = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите Координату Y точки C: ");
                double yC = Convert.ToDouble(Console.ReadLine());
                Point pointC = new Point(xC, yC);

                Console.Write("Введите Координату Х точки D: ");
                double xD = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите Координату Y точки D: ");
                double yD = Convert.ToDouble(Console.ReadLine());
                Point pointD = new Point(xD, yD);

                Quadrilateral quadrilateral = new Quadrilateral(pointA, pointB, pointC, pointD);
                
                Console.WriteLine("Углы четырёхугольника.");
                double angle = quadrilateral.getAngleABC;
                Console.WriteLine($"ABC: {angle}");
                angle = quadrilateral.getAngleBCD;
                Console.WriteLine($"BCD: {angle}");
                angle = quadrilateral.getAngleCDA;
                Console.WriteLine($"CDA: {angle}");
                angle = quadrilateral.getAngleDAB;
                Console.WriteLine($"DAB: {angle}");

                Console.WriteLine("Стороны четырёхугольника.");
                double side = quadrilateral.getSideAB;
                Console.WriteLine($"AB: {side}");
                side = quadrilateral.getSideBC;
                Console.WriteLine($"BC: {side}");
                side = quadrilateral.getSideCD;
                Console.WriteLine($"CD: {side}");
                side = quadrilateral.getSideDA;
                Console.WriteLine($"DA: {side}");

                double perimeter = quadrilateral.perimeterQuadrangle();
                Console.WriteLine($"Периметр четырёхугольника: {perimeter}");

                double square = quadrilateral.square();
                Console.WriteLine($"Площадь четырёхугольника: {square}");

                string type = quadrilateral.type();
                Console.WriteLine($"Тип четырёхугольника: {type}");

                Console.WriteLine("Вы хотите продолжить? (0 - нет, 1 - да)");
                choice = Convert.ToInt32(Console.ReadLine());

            } while (choice != 0);

        }
    }
}
