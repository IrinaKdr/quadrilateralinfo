﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadrilateralInfo
{
    class Vector
    {
        private double m_x;
        private double m_y;

        public Vector(Point a, Point b)
        {
            m_x = b.getX() - a.getX();
            m_y = b.getY() - a.getY();
        }

        public double getX() { return m_x; }
        public double getY() { return m_y; }

        public static double operator *(Vector firstVector, Vector secondVector)
        {
            return ((firstVector.getX()*secondVector.getX())+(firstVector.getY() * secondVector.getY()));
        }

        public double module()
        {
            return Math.Sqrt((m_x * m_x) + (m_y * m_y));
        }
    }
}
