﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadrilateralInfo
{
     class Point
    {
        private double m_x;
        private double m_y;

        public Point(double x = 0, double y = 0)
        {
            m_x = x;
            m_y = y;
        }

        public double getX() { return m_x; }

        public double getY() { return m_y; }

    }
}
