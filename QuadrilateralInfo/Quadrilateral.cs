﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadrilateralInfo
{
    class Quadrilateral
    {
        private Point m_A;
        private Point m_B;
        private Point m_C;
        private Point m_D;

        private Vector vectorAB;
        private Vector vectorBC;
        private Vector vectorCD;
        private Vector vectorDA;

        private double angleABC;
        private double angleBCD;
        private double angleCDA;
        private double angleDAB;

        private double sideAB;
        private double sideBC;
        private double sideCD;
        private double sideDA;

        public Quadrilateral(Point a, Point b, Point c, Point d)
        {
            m_A = a;
            m_B = b;
            m_C = c;
            m_D = d;

            vectorAB = new Vector(m_A, m_B);
            vectorBC = new Vector(m_B, m_C);
            vectorCD = new Vector(m_C, m_D);
            vectorDA = new Vector(m_D, m_A);

            angleABC = (Math.Acos(angleSize(vectorAB, vectorBC)))*(180/Math.PI);
            angleBCD = (Math.Acos(angleSize(vectorBC, vectorCD)))*(180 / Math.PI);
            angleCDA = (Math.Acos(angleSize(vectorCD, vectorDA)))*(180 / Math.PI);
            angleDAB = (Math.Acos(angleSize(vectorDA, vectorAB)))*(180 / Math.PI);

            sideAB = vectorAB.module();
            sideBC = vectorBC.module();
            sideCD = vectorCD.module();
            sideDA = vectorDA.module();

        }

        public double getAngleABC => angleABC;
        public double getAngleBCD => angleBCD;
        public double getAngleCDA => angleCDA;
        public double getAngleDAB => angleDAB;

        public double getSideAB => sideAB;
        public double getSideBC => sideBC;
        public double getSideCD => sideCD;
        public double getSideDA => sideDA;

        private double angleSize(Vector a, Vector b)
        {
            return ((a * b) / (a.module() * b.module()));
        }

        public double perimeterQuadrangle()
        {
            return sideAB + sideBC + sideCD + sideDA;
        }

        public double square()
        {
            double semiPerimeter = (sideAB + sideBC + sideCD + sideDA) / 2;
            return Math.Sqrt((semiPerimeter - sideAB) * 
                (semiPerimeter - sideBC) * 
                (semiPerimeter - sideCD) * 
                (semiPerimeter - sideDA));
        }

        public String type() {
            if (sideAB.Equals(sideBC) &&
                sideAB.Equals(sideCD) &&
                sideAB.Equals(sideDA) &&
                angleABC.Equals(90.0)) {
                return "квадрат";
            }
            else if (sideAB.Equals(sideBC) &&
                sideAB.Equals(sideCD) &&
                sideAB.Equals(sideDA) &&
                !angleABC.Equals(90.0)) {
                return "ромб";
            }
            else if (angleABC.Equals(90.0) &&
                angleBCD.Equals(90.0) &&
                angleCDA.Equals(90.0) &&
                angleDAB.Equals(90.0) && 
                (!sideAB.Equals(sideBC) ||
                !sideAB.Equals(sideCD) ||
                !sideAB.Equals(sideDA))) {
                return "прямоугольник";
            }
            else {
                return "другое";
            }
        }
    }
}
